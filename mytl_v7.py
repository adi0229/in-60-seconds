
from datetime import datetime, timedelta
import time
import logging, os , sys
from bottle import route, run, template, response, request, install , get, post, error
import sqlite3
from sqlite3 import Error


log_file = os.path.join(os.getcwd(), 'mytl_logs.log')
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
	filename = log_file, level = logging.INFO)


class Timelog:

	log = []

	start = datetime.now()

	# time_log structure

	# [ year, month, day, week, start_time, end_time, duration, comment ]


	def start_record(self, comment):


		start_time = self.start.strftime("%H:%M:%S")


		self.log.append(start_time)
		self.log.append(comment)

		logging.info(f"User Start to record for {comment}.")



	def end_record(self, comment):


		end = datetime.now()

		
		year = end.strftime("%Y")
		month = end.strftime("%m")
		week = end.strftime("%W")
		day = end.strftime("%d")
		end_time = end.strftime("%H:%M:%S")
		duration = (end - self.start) / timedelta(microseconds=1000000)


		self.log.insert(0,year)
		self.log.insert(1,month)
		self.log.insert(2,day)
		self.log.insert(3,week)
		self.log.insert(5,end_time)
		self.log.insert(6,duration)

		logging.info(f"User End the recored session for {comment}.")

def db_connect(db_file):

    """
    create a database connection to a SQLite database
    """
    try:
        conn = sqlite3.connect(db_file)
        
        return conn  

    except Error as e:
        print(e)

    return None

def create_table(conn):
    
    try:
        c = conn.cursor()
        create_table_sql = '''CREATE TABLE IF NOT EXISTS timelog
             (id INTEGER PRIMARY KEY AUTOINCREMENT, year text, month text,  day text , week text, start_time text, end_time text,
              duration real, comment text)'''
        c.execute(create_table_sql)
        return c          
    except Error as e:
        print(e)

def insert_log(conn,row):

    sql = ''' INSERT INTO timelog( year, month, day, week, start_time, end_time, duration, comment)
              VALUES(?,?,?,?,?,?,?,?) '''
    c = conn.cursor()
    c.execute(sql,row)
    conn.commit()

def read_log(conn):


    sql = '''SELECT id, duration, comment FROM timelog '''
    c = conn.cursor()
    c.execute(sql)
    raw_data = c.fetchall()
 
    return raw_data 


def get_total_log_num(conn):

    sql = ''' SELECT * FROM timelog ORDER BY id DESC LIMIT 1 '''
    c = conn.cursor()
    c.execute(sql)
    total_log_num = c.fetchall()[0][0]
    #print(type(total_log_num))
    return total_log_num



@get('/timer')
def timer():

	global timelog 

	logging.info("User log in." )

	timelog = Timelog()

	return '''
	<form method = 'POST' >
	计时: <input name = "key", type = "text">
	注释: <input name = "comment", type = "text">
	<input type = 'submit'/>
	</form>
	'''

@route('/timer/logs/<log_id:int>')
def show_log(db, log_id):
	c = db.execute('SELECT id, duration, comment FROM timelog WHERE id = ? ', (log_id,))
	row = c.fetchone()
	return template('''<p>Id: {{id}}</p> <p>Duration: {{duration}}</p> <p>Comment: {{comment}}</p> ''',
		id = row[0], duration = row[1], comment = row[2])



@post('/timer')
def submit():

	global start, end, year, month, day, week, start_time, end_time, comment, duration, conn, c , timelog

	conn = db_connect('my_timelog.db')
	c = create_table(conn) 

	# timelog = Timelog()

	key = request.forms.get('key')

	comment = request.forms.get('comment') 

	logging.info(response.status)

	if key == ',':

		timelog = Timelog()

		timelog.start_record(comment)

		start_time = timelog.log[0]

		return template('''
			<p>开始： {{start_time}}<p>
			<p>注释： {{comment}}<p>
			<form method = 'POST' >
			计时: <input name = "key", type = "text">
			注释: <input name = "comment", type = "text">
			<input type = 'submit'/>
			</form>''', start_time = start_time, comment = comment)

	elif key == '.':

		timelog.end_record(comment)
		row = timelog.log

		end_time = timelog.log[5] 
		duration = timelog.log[6] 

		insert_log(conn, row)

		return template('''
			<p>开始: {{start_time}}</p>
			<p>结束: {{end_time}}</p>
			<p>时长: {{duration}}</p>
			<p>注释: {{comment}}</p>
			<form method = 'POST' >
			计时: <input name = "key", type = "text">
			<input type = 'submit'/>
			</form>''',  start_time = start_time, end_time = end_time , duration = duration, comment = comment)

	elif key == 'sum':

	    total_log_num = get_total_log_num(conn)	

	    return template('''

	    	<p>Sum: {{total_log_num }}</p>

	    	<form method = 'POST' >
			input: <input name = "key", type = "text">
			<input type = 'submit'/>
			</form>''', total_log_num = total_log_num)

@error(403)
@error(404)
def mistake(code):
    return 'Sorry, something wrong.The developer of this web, adi0229, he needs to learn more.'


   
if __name__ == '__main__':

	if os.environ.get('APP_LOCATION') == 'heroku':
		run(host="0.0.0.0", port=int(os.environ.get("PORT", 5000)))
	else:
		run(host='localhost', port=8080, debug=True)




